<?php
/*
	Section: Buttons
	Author: Curtis Grant
	Author URI: http://www.roadsidemultimedia.com
	Description: Semantics Button
	Class Name: SemanticsButtons
	Edition: pro
	Filter: rsbuttons, roadside
*/


class SemanticsButtons extends PageLinesSection {


	function section_styles(){
		
		wp_enqueue_style( 'SemanticsButtons-css', $this->base_url.'/css/semantic.css');
		
	}
	
	function section_head(){

	}

	function section_opts(){
		$options = array();

		$options[] = array(

			'title' => __( 'List Configuration', 'pagelines' ),
			'type'	=> 'multi',
			'opts'	=> array(
				array(
					'key'		=> 's_button_help_vid',
					'type'          => 'link',
            		'url'           => 'https://www.youtube.com/watch?v=cnfs5tb2KyE',
            		'classes'       => 'youtube',
            		'label'		=> __( 'Need Help', 'pagelines' ),
				),
				array(
					'key'		=> 's_button_text',
					'label'		=> __( 'Text on Button', 'pagelines' ),
					'type'		=> 'text',
				),
				array(
					'key'		=> 's_button_link',
					'label'		=> __( 'Link full http url', 'pagelines' ),
					'type'		=> 'text',
				),
				array(
						'type' 			=> 'select',
						'key'			=> 's_button_color',
						'label' 		=> 'Button Color',
						'opts'			=> array(
							'black'		=> array('name' => 'Black'),
							'blue'	=> array('name' => 'Blue'),
							'green'		=> array('name' => 'Green'),						
							'purple'		=> array('name' => 'Purple'),
							'red'	=> array('name' => 'Red'),
							'orange'	=> array('name' => 'Orange'),
							'teal'	=> array('name' => 'Teal'),
							''	=> array('name' => '- - -'),
							'turqoise'	=> array('name' => 'Turqoise'),
							'emerald'	=> array('name' => 'Emerald'),
							'peter-river'	=> array('name' => 'Peter River'),
							'amethyst'	=> array('name' => 'Amethyst'),
							'green-sea'	=> array('name' => 'Green Sea'),
							'nephritis'	=> array('name' => 'Nephritis'),
							'belize-hole'	=> array('name' => 'Belize Hole'),
							'wisteria'	=> array('name' => 'Wisteria'),
							'midnight-blue'	=> array('name' => 'Midnight Blue'),
							'sun-flower'	=> array('name' => 'Sun Flower'),
							'carrot'	=> array('name' => 'Carrot'),
							'alizarin'	=> array('name' => 'Alizarin'),
							'clouds'	=> array('name' => 'Clouds'),
							'concrete'	=> array('name' => 'Concrete'),
							'pumpkin'	=> array('name' => 'Pumpkin'),
							'pomegranate'	=> array('name' => 'Pomegranate'),

						)
					),
				
				
				array(
						'type' 			=> 'select',
						'key'			=> 's_button_size',
						'label' 		=> 'Button Size',
						'opts'			=> array(
							'mini'		=> array('name' => 'Mini'),
							'tiny'		=> array('name' => 'Tiny'),
							'small'	=> array('name' => 'Small'),
							'medium'	=> array('name' => 'Medium'),
							'large'	=> array('name' => 'Large'),
							'big'	=> array('name' => 'Big'),
							'huge'	=> array('name' => 'Huge'),
							'massive'	=> array('name' => 'Massive'),
						)
				),
			)

		);

		return $options;
		
	}
	
	function the_media(){


	}

   function section_template( ) {	
   	$s_button_color = $this->opt('s_button_color') ? $this->opt('s_button_color') : '';
   	$s_button_text = $this->opt('s_button_text') ? $this->opt('s_button_text') : 'I am a Button';
   	$s_button_size = $this->opt('s_button_size') ? $this->opt('s_button_size') : 'medium';
   	$s_button_link = $this->opt('s_button_link') ? $this->opt('s_button_link') : '#';     
	 ?>
	<a href="<?php echo $s_button_link; ?>" class="ui button <?php echo $s_button_size ?> <?php echo $s_button_color ?>">
     <?php echo $s_button_text; ?>
    </a>

<?php }


}